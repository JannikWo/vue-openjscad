```vue
<template>
  <div id="app">
    <h1>Vue OpenJSCAD</h1>
    <open-jscad
      design="gearset.jscad"
      :panel="{ size: 223 }"
      :camera="{
        position: { x: 0, y: 0, z: 150 },
        clip: { min: 1, max: 1000 }
      }"
    ></open-jscad>
  </div>
</template>

<script>
import OpenJscad from "@jwc/vue-openjscad";

export default {
  name: "app",
  components: {
    OpenJscad
  }
};
</script>

<style>
#app {
  font-family: "Avenir", Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  color: #2c3e50;
  margin-top: 5px;
}
</style>
```
